import React, { Component } from 'react';
import Fade from 'react-reveal/Fade';

class Percent extends Component {

  state = {
    discountStart: 0,
    discountEnd: 30
  }

  porcentage = () => {
    if (this.state.discountStart < this.state.discountEnd) {
      this.setState({
        discountStart: this.state.discountStart + 1
      })
    }
  }

  componentDidUpdate() {
    setTimeout(() => {
      this.porcentage()
    }, 30)
  }

  render() {
    return (
      <Fade
        onReveal={() => this.porcentage()}
      >
        <div className="highlights-box-percent">
          <p><span>{this.state.discountStart}</span>%</p>
        </div>
      </Fade>
    )
  }
}

export default Percent;