import React from 'react';
import Percent from './Percent';
import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';

const Highlights = () => {
  return (
    <div className="highlights">
      <Fade>
        <div className="highlights-header">
          <h2>HIGHLIGHTS</h2>
        </div>
      </Fade>
      <Fade>
        <div className="highlights-text">
          <p>Quisque quis tincidunt turpis. Quisque in finibus ante, nec semper erat. Suspendisse eu ultricies purus. Nunc rhoncus in diam vel porta. Aliquam lectus dolor, ullamcorper in rhoncus et, ultrices non dolor. Sed luctus lectus diam, eget tempor leo dictum non. Nunc venenatis viverra lorem sit amet vehicula. Pellentesque mollis eget est eu luctus. Aliquam nec convallis nibh. Curabitur pretium, lectus vel interdum aliquam, ligula libero dignissim tortor, non pharetra massa ex a diam. Nulla suscipit pharetra euismod. Suspendisse eu velit et est fermentum hendrerit in sed nisi. Etiam sapien libero, venenatis non porttitor id, feugiat at nunc. In hac habitasse platea dictumst. Aenean vulputate lectus diam, sit amet fermentum ipsum placerat sollicitudin.</p>
        </div>
      </Fade>
      <div className="highlights-box">
        <Percent />
        <Slide right>
          <div className="highlights-box-text">
            <h3>Sed nec dictum elit</h3>
            <p>Nulla at dolor blandit, volutpat turpis in, tempor elit. Quisque in neque convallis, tempus nunc sit amet, fringilla eros. Nam facilisis porta consequat. Phasellus pretium sagittis eleifend. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus et feugiat tellus, ut tincidunt enim. Integer sodales lacinia arcu, vitae porttitor nunc euismod in. Donec vel interdum neque. Sed consequat, sapien id varius pellentesque, massa ante ornare odio, pellentesque mollis quam velit quis sapien. In pulvinar nulla in pulvinar lacinia. Proin vehicula nulla risus, vel porta erat convallis eu. Nam eros nulla, pellentesque sit amet magna ut, vulputate feugiat felis. Morbi ut imperdiet metus, vel congue augue. Suspendisse id enim tellus. Aenean facilisis ex mollis tellus sollicitudin ultricies in quis arcu. Proin vel mauris nibh. Aenean tempor turpis tellus, ultricies eleifend tortor aliquet sed. Pellentesque scelerisque aliquam finibus. Maecenas scelerisque augue sed ligula facilisis tristique. Suspendisse potenti. Cras eu tellus orci.</p>
          </div>
        </Slide>
      </div>
    </div>
  );
};

export default Highlights;