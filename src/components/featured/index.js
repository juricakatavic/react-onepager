import React from 'react';
import Carrousel from './Carrousel';
import Timer from './Timer';

const Featured = () => {

  return (
    <div>
      <Carrousel />
      <div className="jumbotron">
        <h1>JUMBOTRON</h1>
        <Timer />
      </div>
    </div>
  );
};

export default Featured;