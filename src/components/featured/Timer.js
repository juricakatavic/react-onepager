import React, { Component } from 'react';

class Timer extends Component {

  state = {
    deadline: 'Dec, 16, 2021',
    days: '0',
    hours: '0',
    minutes: '0',
    seconds: '0'
  }

  getTimeUntil(deadline) {
    const time = Date.parse(deadline) - Date.parse(new Date());
    if (time < 0) {
      console.log('date passed')
    } else {
      const seconds = Math.floor((time / 1000) % 60);
      const minutes = Math.floor((time / 1000 / 60) % 60);
      const hours = Math.floor((time / (1000 * 60 * 60)) % 24);
      const days = Math.floor(time / (1000 * 60 * 60 * 24));

      this.setState({
        days,
        hours,
        minutes,
        seconds
      })
    }
  }

  componentDidMount() {
    setInterval(() => this.getTimeUntil(this.state.deadline), 1000)
  }

  render() {
    return (
      <div className="jumbotron-timer">
        <div>
          {this.state.days} d
                </div>
        <div>
          {this.state.hours} h
                </div>
        <div>
          {this.state.minutes} m
                </div>
        <div>
          {this.state.seconds} s
                </div>
      </div>
    );
  }
}

export default Timer;