import React from 'react';
import Slider from "react-slick";

import random1 from '../../resources/images/random1.jpg';
import random2 from '../../resources/images/random2.png';
import random3 from '../../resources/images/random3.png';

const Carrousel = () => {

  const settings = {
    dots: false,
    infinite: true,
    autoplay: true,
    speed: 500
  }

  return (
    <div
      style={{
        height: `${window.innerHeight}px`,
        overflow: 'hidden'
      }}
    >
      <Slider {...settings} >
        <div>
          <div
            className="carrousel_image"
            style={{
              background: `url(${random1})`,
              height: `${window.innerHeight}px`
            }}
          >
          </div>
        </div>
        <div>
          <div
            className="carrousel_image"
            style={{
              background: `url(${random2})`,
              height: `${window.innerHeight}px`
            }}
          >
          </div>
        </div>
        <div>
          <div
            className="carrousel_image"
            style={{
              background: `url(${random3})`,
              height: `${window.innerHeight}px`
            }}
          >
          </div>
        </div>
      </Slider>

    </div>
  );
};

export default Carrousel;