import React, { Component } from 'react';
import Zoom from 'react-reveal/Zoom';

class Pricing extends Component {

  state = {
    prices: [100, 150, 200],
    positions: ['UP', 'DOWN', 'LEFT'],
    description: [
      'Quisque quis tincidunt turpis. Quisque in finibus ante, nec semper erat. Suspendisse eu ultricies purus. Nunc rhoncus in diam vel porta. Aliquam lectus dolor, ullamcorper in rhoncus et, ultrices non dolor.',
      'Sed luctus lectus diam, eget tempor leo dictum non. Nunc venenatis viverra lorem sit amet vehicula. Pellentesque mollis eget est eu luctus. Aliquam nec convallis nibh. Curabitur pretium, lectus vel interdum aliquam, ligula libero dignissim tortor, non pharetra massa ex a diam.',
      'Nulla suscipit pharetra euismod. Suspendisse eu velit et est fermentum hendrerit in sed nisi. Etiam sapien libero, venenatis non porttitor id, feugiat at nunc. In hac habitasse platea dictumst. Aenean vulputate lectus diam, sit amet fermentum ipsum placerat sollicitudin.'
    ],
    delay: [500, 0, 500]
  }

  showBoxes = () => (
    this.state.prices.map((box, i) => (
      <Zoom delay={this.state.delay[i]} key={i}>
        <div className="pricing-box">
          <div className="pricing-box-number">
            <span>{this.state.prices[i]}</span>
          </div>
          <div className="pricing-box-header">
            <span>{this.state.positions[i]}</span>
          </div>
          <div className="pricing-box-description">
            <span>{this.state.description[i]}</span>
          </div>
        </div>
      </Zoom>
    ))
  )

  render() {
    return (
      <div className="pricing">
        {this.showBoxes()}
      </div>
    );
  }
}

export default Pricing