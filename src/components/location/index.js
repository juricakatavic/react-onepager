import React from 'react';

const Location = () => {
  return (
    <div className="location">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2934.966321539903!2d18.107584965699257!3d42.640873925313414!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x134c0b32f93c5965%3A0xdaab5b77b07f1455!2sOld+Town%2C+20000%2C+Dubrovnik!5e0!3m2!1shr!2shr!4v1566206215247!5m2!1shr!2shr"
        width="100%"
        title="map of dubrovnik"
        height="600"
        frameBorder="0"
        allowFullscreen></iframe>
    </div>
  );
};

export default Location;