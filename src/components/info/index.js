import React from 'react';
import Zoom from 'react-reveal/Zoom';

const Info = () => {
  return (
    <div className="info">
      <Zoom duration={500}>
        <div className="info-box">
          <h2>Sed id</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non porttitor leo. Nam vestibulum placerat magna, quis condimentum lorem volutpat et. Donec tristique mattis ex, a cursus urna bibendum quis. Ut cursus ante eu felis tristique, eu feugiat justo consequat. Aliquam vitae tempus nisl, non tempor erat. Vestibulum auctor quis massa eget elementum. Aliquam lobortis justo ac tellus euismod, ut rhoncus neque placerat. Ut vitae lectus enim. Quisque arcu quam, euismod et massa et, finibus facilisis nulla. Quisque a turpis mi. Maecenas luctus dolor vel libero mattis, in auctor urna sagittis.</p>
        </div>
      </Zoom>
      <Zoom duration={500}>
        <div className="info-box">
          <h2>Curabitur volutpat</h2>
          <p>Quisque quis tincidunt turpis. Quisque in finibus ante, nec semper erat. Suspendisse eu ultricies purus. Nunc rhoncus in diam vel porta. Aliquam lectus dolor, ullamcorper in rhoncus et, ultrices non dolor. Sed luctus lectus diam, eget tempor leo dictum non. Nunc venenatis viverra lorem sit amet vehicula. Pellentesque mollis eget est eu luctus. Aliquam nec convallis nibh. Curabitur pretium, lectus vel interdum aliquam, ligula libero dignissim tortor, non pharetra massa ex a diam. Nulla suscipit pharetra euismod. Suspendisse eu velit et est fermentum hendrerit in sed nisi. Etiam sapien libero, venenatis non porttitor id, feugiat at nunc. In hac habitasse platea dictumst.</p>
        </div>
      </Zoom>
    </div>
  );
};

export default Info;